# SharedSpace

Espaço compartilhado localmente.

Projeto ainda em desenvolvimento com objetivo de fazer o backup de arquivos entre dispositivos de maneira simples.

Será utilizado tanto para fins de segurança (guardar múltiplas cópias de um mesmo arquivo) quanto para diminuir o consumo do armazenamento (mover arquivos para outro dispositivo através da internet).

Bibliotecas e tecnologias utlizadas:

- Node.js
- Express
- Multer
- Firebase Auth
- Firebase Realtime Database
- Firebase Storage
- JQuery
- Fetch
- Bootstrap
- Bootstrap Icons
- Feather Icons

O projeto consiste em uma API REST que recebe solicitações do usuário através de uma página web, e se comunica com os serviços do Firebase para gerenciamento de usuários, armazenamento de arquivos e metadados.

# Projeto em Funcionamento

O projeto não pode ser executado fora do meu ambiente de trabalho, mas brevemente pretendo deixar a página aberta para acesso externo. Assim que concluir o básico para uso da aplicação, estarei disponibilizando o link de acesso através deste documento.

## Prints

De momento, fique com alguns prints do trabalho em andamento:

![Página de Login](https://i.imgur.com/6ld8gAK.png)

![Página principal](https://i.imgur.com/2kjsKGf.png)

![Página principal responsiva](https://i.imgur.com/wCMb3p7.png)