const firebase = require('firebase')
const firebaseAuth = require('firebase').auth()
const firebaseDB = firebase.database()
const fs = require('fs')

firebaseAuth.useDeviceLanguage()
require('firebase/storage')

exports.addFile = async (req, res) => {
    try {
        //Set the files properties, like path and size
        let fileProps = ''
        req.files['file'] ? fileProps = req.files['file'] : fileProps = req.files['folder']

        let userDB = firebaseAuth.currentUser

        if (!userDB)
            return res.status(403).send({
                message: "User not logged in"
            })
        if (!fileProps)
            return res.status(422).send({
                message: "File was not sent"
            })

        //Fills filePaths array with the all the files paths (fileName or folder/fileName)
        let filePaths = []
        if (typeof req.body.filePath === 'undefined') { //File(s), there is no filePath
            for (val of fileProps)
                filePaths.push(val.originalname)
        }
        else { //Folder
            if (typeof req.body.filePath === 'string') { //Single file in folder, single value in filePath
                filePaths.push(req.body.filePath)
            }
            else { //Multiple files in folder, multiple values in filePath
                for (val of req.body.filePath)
                    filePaths.push(val)
            }
        }

        //Function to upload meta-data to Firebase Database
        const uploadMetaData = async (fileName, fileSize) => {
            console.log('teste')
            let DBRef = firebase.database().ref(`/files/${userDB.uid}`).push()
            await DBRef.set({ file: fileName, size: fileSize })
        }

        //Upload all files to Firebase Storage
        let count = 0 //Count will serve as a ref. for each file in filePaths
        for (fileProp of fileProps) {
            console.log(typeof fileProp.size === 'string')
            console.log(typeof fileProp.size === 'number')
            let file = fs.readFileSync(fileProp.path) //Read temp file
            let StorageRef = firebase.storage().ref(`/files/${userDB.uid}/${filePaths[count]}`)

            await StorageRef.getDownloadURL()
                .then().catch(async () => { //Check if file is not stored already
                    await StorageRef.put(file)
                    await uploadMetaData(filePaths[count], fileProp.size)
                })
            fs.unlinkSync(fileProp.path) //Delete temp file
            count++
        }
        return res.status(200).send({
            message: "Files sent successfully!",
            status: 'success'
        })
    } catch (e) {
        return res.status(500).send({
            message: e.message,
            status: 'danger'
        })
    }
}

exports.listFile = async (req, res) => {
    try {
        let userDB = firebaseAuth.currentUser
        console.log('listFile')
        if (!userDB)
            return res.status(403).send({
                message: "User not logged in"
            })

        let DBRef = firebaseDB.ref(`/files/${userDB.uid}`)

        DBRef.once('value').then(async (snapshot) => {
            console.log('listFile snapshot')
            if (snapshot.val() == null) {
                return res.status(200).send({
                    message: "User has no files"
                })
            }
            else {
                let filesList = []
                snapshot.forEach(fileKey => {
                    let fileName = fileKey.val().file

                    let fileSize = []

                    for (num of fileKey.val().size.toString()) {
                        fileSize.push(num)
                    }

                    if (fileSize.length < 4) {
                        fileSize = `${fileSize.toString()} B`
                    }
                    else if (fileSize.length < 7) {
                        let i = 3
                        while (i--)
                            fileSize.pop()

                        fileSize = `${fileSize.join('')} KB`
                    }
                    else if (fileSize.length > 7) {

                        fileSize.splice(fileSize.length - 6)
                        fileSize = `${fileSize.join('')} MB`
                    }
                    filesList.push([fileName, fileSize])
                })
                return res.status(200).send({
                    message: filesList
                })
            }
        })
    } catch (e) {
        return res.status(500).send({
            message: e.message
        })
    }
}

exports.deleteFile = async (req, res) => {
    try {
        const { file } = req.body

        let userDB = firebaseAuth.currentUser
        if (!userDB)
            return res.status(403).send({
                message: "User not logged in"
            })

        let DBRef = firebaseDB.ref(`/files/${userDB.uid}`)
        let storageRef = firebase.storage().ref(`/files/${userDB.uid}/${file}`)

        DBRef.once('value').then(async (snapshot) => {
            if (snapshot.val() == null) {
                return res.status(404).send({
                    message: 'File not found',
                    status: 'danger'
                })
            }
            else {
                snapshot.forEach(instance => {
                    let fileName = instance.val().file
                    if (fileName == file) {
                        const DBFileRef = DBRef.child(instance.key)
                        DBFileRef.remove()
                    }
                })
            }
        })
        storageRef.delete().then(() => {
            return res.status(200).send({
                message: 'File deleted successfully!',
                status: 'success'
            })
        }).catch(() => {
            return res.status(500).send({
                message: 'File not found',
                status: 'danger'
            })
        })

    } catch (e) {
        return res.status(500).send({
            message: e.message
        })
    }
}

exports.downloadFile = async (req, res) => {
    try {
        const { fileName } = req.params
        let userDB = firebaseAuth.currentUser

        if (!userDB)
            return res.status(403).send({
                message: "User not logged in"
            })
        const fileRef = firebase.storage().ref(`/files/${userDB.uid}/${fileName}`)

        await fileRef.getDownloadURL().then((url) => {
            return res.status(200).send({
                url: url
            })
        })

    } catch (e) {
        return res.status(500).send({
            message: e.message
        })
    }
}