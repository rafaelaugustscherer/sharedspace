const firebase = require('firebase')
const admin = require('firebase-admin')
const firebaseAuth = firebase.auth()

firebaseAuth.useDeviceLanguage()

const isMissingField = (res, email, pass) => {
    if (!email || !pass)
        return res.status(422).send({
            message: "Missing field"
        })
    return false
}

const isUserOff = (res) => {
    const userDB = firebaseAuth.currentUser

    if (userDB)
        return false
    else
        return res.status(403).send({
            message: "User not logged in.",
            status: "warning"
        })
}

exports.createUser = async (req, res) => {
    try {
        const { email, pass } = req.body

        if (isMissingField(res, email, pass))
            return

        await firebaseAuth.createUserWithEmailAndPassword(email, pass)

        return res.status(200).send({
            message: "User created with success!",
            status: "success"
        })
    } catch (e) {
        return res.status(500).send({
            message: e.message,
            status: "danger"
        })
    }
}

exports.editUser = async (req, res) => {
    try {
        const { name, photoURL, phone, email, pass, emailNeo, passNeo, credChange } = req.body
        const userDB = firebaseAuth.currentUser

        if (isUserOff(res))
            return

        if (name)
            userDB.updateProfile({ displayName: name })
        else if (photoURL)
            userDB.updateProfile({ photoURL: photoURL })
        else if (phone)
            userDB.updatePhoneNumber(phone)
        else if (email && pass) {
            const credential = firebase.auth.EmailAuthProvider.credential(email, pass)
            await userDB.reauthenticateWithCredential(credential)

            if (emailNeo)
                await userDB.updateEmail(emailNeo)
            else if (passNeo)
                await userDB.updatePassword(passNeo)
        }

        return res.status(200).send({
            message: "Ok",
            status: "success"
        })
    } catch (e) {
        return res.status(500).send({
            message: e.message
        })
    }
}

exports.deleteUser = async (req, res) => {
    try {
        const { email, pass } = req.body
        const userDB = firebaseAuth.currentUser

        if (isMissingField(res, email, pass))
            return
        if (isUserOff(res))
            return

        const credential = firebase.auth.EmailAuthProvider.credential(email, pass)
        await userDB.reauthenticateWithCredential(credential)
        await firebaseAuth.signOut()
        await userDB.delete()

        return res.status(200).send({
            message: "Ok",
            status: "success"
        })
    } catch (e) {
        return res.status(500).send({
            message: e.message
        })
    }
}

exports.login = async (req, res) => {
    try {
        const { email, pass } = req.body
        const userDB = firebaseAuth.currentUser

        if (isMissingField(res, email, pass))
            return
        if (userDB)
            return res.status(200).send({
                message: "User already logged in",
                status: "warning"
            })
        await firebaseAuth.signInWithEmailAndPassword(email, pass)

        admin.auth().createCustomToken(firebaseAuth.currentUser.uid)
            .then(token => {
                firebaseAuth.signInWithCustomToken(token)
            })
        return res.status(200).send({
            message: "Logged in",
            status: "success"
        })

    } catch (e) {
        return res.status(500).send({
            message: e.message,
            status: "danger"
        })
    }
}

exports.logout = async (req, res) => {
    try {
        await firebaseAuth.signOut()
        return res.status(200).send({
            message: "Logged out",
            status: "success"
        })
    } catch (e) {
        return res.status(500).send({
            message: e.message,
            status: "danger"
        })
    }
}

exports.userName = async (req, res) => {
    const userDB = firebaseAuth.currentUser

    if (!userDB)
        return res.status(202).send({
            message: null
        })
    else if (userDB.displayName)
        return res.status(200).send({
            message: userDB.displayName
        })
    else
        return res.status(200).send({
            message: userDB.email
        })
}