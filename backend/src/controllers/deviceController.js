const firebase = require('firebase')
const admin = require('firebase-admin')
const firebaseAuth = firebase.auth()
const firebaseDB = firebase.database()

firebaseAuth.useDeviceLanguage

const isUserOff = (res) => {
    const userDB = firebaseAuth.currentUser

    if (userDB)
        return false
    else
        return res.status(403).send({
            message: "User not logged in."
        })
}

exports.addDevice = async (req, res) => {
    try {
        const { devName, macADD } = req.body
        const userDB = firebaseAuth.currentUser

        if (isUserOff(res))
            return

        const devicesRef = firebaseDB.ref('devices/' + userDB.uid)

        await devicesRef.once("value").then((snapshot) => {
            if (snapshot.hasChild(devName)) {
                return res.status(409).send({
                    message: "Device name is already used by another device."
                })
            }
            else if (snapshot.child(devName + '/macADD').val = macADD) {
                return res.status(409).send({
                    message: "Device is already registered."
                })
            }
            else {
                const neoDeviceRef = devicesRef.child(devName)
                neoDeviceRef.set({ macADD: macADD })

                return res.status(200).send({
                    message: "Ok"
                })
            }
        })
    } catch (e) {
        return res.status(500).send({
            message: e.message
        })
    }
}

exports.deleteDevice = async (req, res) => {
    try {
        const { devName } = req.body
        const userDB = firebaseAuth.currentUser

        if (isUserOff(res))
            return

        const devRef = firebaseDB.ref('devices/' + userDB.uid).child(devName)
        await devRef.remove()

        return res.status(200).send({
            message: "Ok"
        })
    } catch (e) {
        return res.status(500).send({
            message: e.message
        })
    }
}