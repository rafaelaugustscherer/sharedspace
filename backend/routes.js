const express = require('express')
const multer = require('multer')
const routes = express.Router()

const upload = multer({dest: 'backend/uploads/'})

const userController = require('./src/controllers/userController')
const deviceController = require('./src/controllers/deviceController')
const fileController = require('./src/controllers/fileController')

//User Routes
routes.post('/user', upload.none(), userController.createUser)
routes.put('/user', upload.none(), userController.editUser)
routes.delete('/user', upload.none(), userController.deleteUser)
routes.post('/user/login', upload.none(), userController.login)
routes.delete('/user/login', upload.none(), userController.logout)
routes.get('/user', upload.none(), userController.userName)

//Device Routes
routes.post('/device', upload.none(), deviceController.addDevice)
routes.delete('/device', upload.none(), deviceController.deleteDevice)

//File Routes
routes.post('/file', upload.fields([{name: 'file'}, {name: 'folder'}]), fileController.addFile)
routes.get('/file', upload.none(), fileController.listFile)
routes.delete('/file', upload.none(), fileController.deleteFile)
routes.get('/file/download/:fileName', upload.none(), fileController.downloadFile)

module.exports = routes