const cors = require('cors')
const express = require('express')
const firebase = require('firebase/app')
const admin = require('firebase-admin')

const firebaseConfig = {
  apiKey: "AIzaSyCAbdBeIHwm5iwNDxMlNOQIajBmLJsQejo",
  authDomain: "sharedspace01.firebaseapp.com",
  databaseURL: "https://sharedspace01.firebaseio.com",
  projectId: "sharedspace01",
  storageBucket: "sharedspace01.appspot.com",
  messagingSenderId: "893804893649",
  appId: "1:893804893649:web:c29498dbe71b642c27bcf0",
  measurementId: "G-34M8EK4R7E"
};
firebase.initializeApp(firebaseConfig)

const adminConfig = {
  credential: admin.credential.applicationDefault(),
  databaseURL: "https://sharedspace01.firebaseio.com",
  storageBucket: "sharedspace01.appspot.com"
}
admin.initializeApp(adminConfig)

const server = express()
const routes = require('./routes')

server.use(express.static('public'))
server.use(cors())
server.use(express.urlencoded({
    limit: '50mb',
    extended: true
}))
server.use(express.json({
    limit: '50mb',
    extended: true
}))
server.use(routes)
server.listen(3003)







