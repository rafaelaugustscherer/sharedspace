
//If user is not logged in, doesn't user menu and sidebar
fetch(`http://localhost:3003/user`, {
    method: 'GET'
}).then((res) => {
    res.json().then(resJSON => {
        console.log(resJSON)
        if (!resJSON.message) {
            $('#userDropdown').removeClass('show')
            $('#menu-toggle').removeClass('show')
        }
        else {
            $.get('/assets/elements/sidebar.html', (data) => {
                $('#sidebar-placeholder').replaceWith(data)
            })
        }
    })
}).catch((err) => {
    console.log(err)
})

//When sidebar menu clicked, toggle sidebar
$('#menu-toggle').click(() => {
    $('#wrapper').toggleClass('toggled')
})


//Collapsable elements for mobile listener
const maxWidthCollapse = window.matchMedia('(max-width: 768px)')

const collapse = e => {
    if (e.matches){
        $('#collapseUserDropDown').removeClass('show')
    }
    else {
        $('#collapseUserDropDown').addClass('show')
    }
    
}
maxWidthCollapse.addListener(collapse)

if (maxWidthCollapse.matches){
    $('#collapseUserDropDown').removeClass('show')
}