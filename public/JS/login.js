const loginForm = document.getElementById('loginForm')

const login = (e) => {
    e.preventDefault()

    let formData = new FormData(loginForm)
    fetch('http://localhost:3003/user/login', {
        method: 'POST',
        body: formData
    }).then(function (response) {
        response.json().then(resJSON => {
            $(alertGenerator(resJSON.message, resJSON.status)).insertBefore(loginForm)
            if (response.ok)
                window.location.href = '/index.html'
        })
    }).catch((err) => {
        alert(err)
    })
}
loginForm.addEventListener('submit', (e) => login(e))
