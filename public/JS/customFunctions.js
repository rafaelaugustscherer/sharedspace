//Replace nav-placeholder with the actual navbar

$.get('/assets/elements/navbar.html', (data) => {
    $('#nav-placeholder').replaceWith(data)
})

//General use functions
const alertGenerator = (message, status) => {
    let alertHTML = `<div class="alert alert-${status} alert-dismissible fade show" role="alert">
                        ${message}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>`
    return alertHTML
    //$(alertGenerator(resJSON.message, resJSON.status)).insertBefore(filesList)
}

const fileListItemGenerator = (file, filesList) => {
    let fileName = file[0]
    let fileSize = file[1]
    filesList.innerHTML +=
        `<li class="list-group-item list-group-item-action d-flex justify-content-between align-items-center">
        ${fileName} - ${fileSize}
        <div>
            <button onclick="javascript:downloadFile('${fileName}')" class="btn col-auto" title="Download do arquivo">
                <img src="assets/feather/download.svg" class="icon">
            </button>
            <button onclick="javascript:deleteFile('${fileName}')" class="btn col-auto" title="Excluir arquivo">
                <img src="assets/feather/trash-2.svg" class="icon">
            </button>
        </div>
    </li>`
}

const logout = () => {

    fetch('http://localhost:3003/user/login', {
        method: 'DELETE'
    }).then(function (response) {
            window.location.href = '/pages/login.html'
    }).catch((err) => {
        alert(err)
    })
}



$(document).ready(() => {
    $('body').show()
});