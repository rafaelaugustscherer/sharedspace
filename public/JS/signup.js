const signUpForm = document.getElementById('signUpForm')

const signUp = (e) => {
    e.preventDefault()
    $('.alert').alert('close')

    if ($('input[name="pass"]').val() != $('input[name="pass-confirm"]').val()) {
        $(alertGenerator('As senhas informadas não coincidem', 'danger')).insertBefore(signUpForm)
        return
    }

    let formData = new FormData(signUpForm)
    fetch('http://localhost:3003/user', {
        method: 'POST',
        body: formData
    }).then(function (response) {
        response.json().then(resJSON => {
            $(alertGenerator(resJSON.message, resJSON.status)).insertBefore(signUpForm)
            if (response.ok){
                setTimeout(() => {window.location.href = '/pages/login.html'}, 2000)
            }
        })
    }).catch((err) => {
        alert(err)
    })
}
signUpForm.addEventListener('submit', (e) => signUp(e))