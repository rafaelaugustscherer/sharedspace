const fileInput = $('#file')
const folderInput = $('#folder')
const fileInputBtn = $('#fileBtn')
const folderInputBtn = $('#folderBtn')
const filesList = document.getElementById('filesList')

const postFile = e => {
    e.preventDefault()
    $('.alert').alert('close')

    let formData = new FormData()
    console.log(postFileForm.file.files)
    console.log(postFileForm.folder.files)

    //File(s) upload
    if (fileInput.val()) {
        for (file of postFileForm.file.files) {
            formData.append('file', file)
        }
        fileInput.val(null)
    }
    //Folder upload
    else if (folderInput.val()) {
        for (file of postFileForm.folder.files) {
            formData.append('folder', file)
            formData.append('filePath', file.webkitRelativePath)
        }
        folderInput.val(null)
    }

    fetch('http://localhost:3003/file', {
        method: 'POST',
        body: formData
    }).then((res) => {
        res.json().then(resJSON => {
            $(alertGenerator(resJSON.message, resJSON.status)).insertBefore(filesList)
        })
        loadFiles()
    }).catch((err) => {
        $(alertGenerator(err, 'danger')).insertBefore(filesList)
    })
}

const loadFiles = () => {
    fetch('http://localhost:3003/file', {
        method: 'GET'
    }).then((res) => {
        if (res.status == 403)
            window.location.href = '/pages/login.html'
        else {
            res.json().then(resJSON => {
                if (typeof resJSON.message === 'object') {
                    if (!res.ok) {
                        alert(res.status + ': ' + resJSON.message)
                    }
                    else {
                        filesList.innerHTML = ''
                        resJSON.message.forEach(file => {
                            fileListItemGenerator(file, filesList)
                        })
                    }
                }
                else {
                    filesList.innerHTML = '<li class="list-group-item list-group-item-action">Usuário não tem arquivos.</li>'
                }
            })
        }
    }).catch((err) => {
        $(alertGenerator(err, 'danger')).insertBefore(filesList)
    })
}

const downloadFile = file => {
    fetch(`http://localhost:3003/file/download/${file}`, {
        method: 'GET'
    }).then((res) => {
        if (res.status == 403)
            window.location.href = '/pages/login.html'
        else {
            res.json().then(resJSON => {
                window.location.href = resJSON.url
            })
        }
    }).catch((err) => {
        $(alertGenerator(err, 'danger')).insertBefore(filesList)
    })
}

const deleteFile = file => {
    $('.alert').alert('close')

    let formData = new FormData()
    formData.append('file', file)

    fetch(`http://localhost:3003/file`, {
        method: 'DELETE',
        body: formData
    }).then((res) => {

        if (res.status == 403)
            window.location.href = '/pages/login.html'
        else {
            res.json().then(resJSON => {
                $(alertGenerator(resJSON.message, resJSON.status)).insertBefore(filesList)
            })
            loadFiles()
        }
    }).catch((err) => {
        $(alertGenerator(err, 'danger')).insertBefore(filesList)
    })
}
//Buttons to upload files. They will lead to a click on input
fileInputBtn.click(() => fileInput.click())
folderInputBtn.click(() => folderInput.click())

//When files are selected, they'll be posted
fileInput.change(e => postFile(e))
folderInput.change(e => postFile(e))

$(document).ready(() => {
    loadFiles()
});

